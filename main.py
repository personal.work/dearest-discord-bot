import discord
import os
from discord.ext import commands
from dotenv import load_dotenv
from commands import MainCog

load_dotenv()


intents = discord.Intents.all()
intents.members = True
intents.messages = True
client = commands.Bot(command_prefix="$", intents=intents)

DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")


@client.event
async def on_ready():
    print(f"{client.user} is online and ready to rumble!")
    await client.add_cog(MainCog(client, "Love"))


client.run(DISCORD_TOKEN)
