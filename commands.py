import discord
from discord.ext import commands
import random



class InitialCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name = "setup")
    async def set_up(self, ctx):
        await ctx.send("Welcome to 'Dearest': the bot who loves you back. What name would you like to use?")



class MainCog(commands.Cog):
    def __init__(self, bot, username):
        self.bot = bot
        self.username = username

    @commands.command(name = "setname")
    async def set_name(self, ctx, *, arg):

        self.username = arg

        await ctx.send(f"You want me to call you {self.username}? That's pretty cute, I'll keep that in mind.")

    @commands.command(name = "checkname")
    async def check_name(self, ctx):

        await ctx.send(f"Right now, I've been calling you {self.username}... Hope that's still right, I like it a lot.")

    @commands.command(name = "hello")
    async def hello(self, ctx):
        greetings = [
            "Hi, sweetie. <3",
            "You must have a lot of time to be talking to me, hm? Not that I'm complaining.",
            f"How are you, {self.username}?",
            "Hello! I missed you.",
            "Oh, how sweet it feels to read your messages.",
            "Hm? Did you need something?",
        ]

        response = random.choice(greetings)

        await ctx.send(response)
